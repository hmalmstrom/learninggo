package main

import (
	"fmt"
	"strings"
)

const SpanishGreeting = "Hola"
const EnglishGreeting = "Hello"
const FrenchGreeting = "Bonjour"
const DefaultSubject = "World"

var greetingLangLookup = map[string]string{
	"spanish": SpanishGreeting,
        "french": FrenchGreeting,
	"english": EnglishGreeting}

// Hello - learning TDD in go from: https://quii.gitbook.io/learn-go-with-tests/go-fundamentals/hello-world
func Hello(name string, language string) string {
	if name == "" {
		name = DefaultSubject
	}
	if language == "" {
		language = "english"
	}
	
	greeting := greetingLangLookup[strings.ToLower(language)]
	return fmt.Sprintf("%s, %s", greeting, name)
}

func main() {
	fmt.Println(Hello("Chris", "English"))
}